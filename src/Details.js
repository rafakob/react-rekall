import React, { useEffect, useState } from "react";
import { fetchHeartbeats } from "./Api.js";
import { Spin } from "antd";
import Crossfilter from "./Crossfilter";

const Details = ({ id }) => {
  const [heartbeats, setHeartbeats] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // fetchHeartbeats(
    //   "1595249519419-731476396ec6-1gy6ofah5oaqi10vijwceht5f3-5bde11"
    // ).then((response) => {
      fetchHeartbeats(id).then((response) => {
      setHeartbeats(response);
      setLoading(false);
    }, console.error);
  }, []);

  return (
    <div className="heartbeatsContainer">
      <h1>{id}</h1>
      {loading ? (
        <Spin size="large" />
      ) : (
        <div className="heartbeats">
          <h2>{`Heartbeats count: ${heartbeats.length}`}</h2>
          <Crossfilter heartbeats={heartbeats} />
          {heartbeats.map((item, index) => (
            <div key={index}>
              <h2>{`Heartbeat ${index}`}</h2>
              <pre>
                <code>{JSON.stringify(JSON.parse(item), null, 2)}</code>
              </pre>
            </div>
          ))}
        </div>
      )}
    </div>
  );
};

export default Details;
