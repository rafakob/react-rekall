import React, { useEffect, useState } from "react";
import { navigate } from "@reach/router";
import { fetchSessions } from "./Api.js";
import { Table, Tag, Space } from "antd";

// const dataSource = [
//   {
//     key: '1',
//     name: 'Mike',
//     age: 32,
//     address: '10 Downing Street',
//   },
//   {
//     key: '2',
//     name: 'John',
//     age: 42,
//     address: '10 Downing Street',
//   },
// ];

const columns = [
  {
    title: "Session",
    dataIndex: "session",
    key: "session",
  },
  {
    title: "User",
    dataIndex: "user",
    key: "user",
  },
  {
    title: "Datetime",
    dataIndex: "datetime",
    key: "datetime",
  },
  {
    title: "Action",
    key: "action",
    render: (text, record) => (
      <Space size="middle">
        {/*<a>Invite {record.name}</a>*/}
        <a>Delete</a>
      </Space>
    ),
  },
];

function parseUser(sessionId) {}

const Sessions = () => {
  const [sessions, setSessions] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setSessions([]);
    setLoading(true);

    fetchSessions().then((response) => {
      const sessionsSource = response.map((item) => ({
        key: item.id,
        session: item.id,
        user: item.user,
        datetime: item.datetime,
      }));

      setLoading(false);
      setSessions(sessionsSource);
    }, console.error);
  }, []);

  return (
    <div className="sessionsList">
      <Table
        dataSource={sessions}
        columns={columns}
        loading={loading}
        pagination={{
          pageSizeOptions: ["100", "200", "500"],
          showSizeChanger: true,
          defaultPageSize: 100,
          locale: { items_per_page: "" },
        }}
        onRow={(record, rowIndex) => {
          return {
            onClick: (event) => {
              navigate(`/details/${record.session}`);
            },
          };
        }}
      />
    </div>
  );
};

export default Sessions;
