import React from "react";
import { render } from "react-dom";
import { Link, Router } from "@reach/router";
import Sessions from "./Sessions";
import Details from "./Details";
import "antd/dist/antd.css";

const App = () => {
  return (
    <div>
      <Router>
        <Sessions path="/" />
        <Details path="/details/:id" />
      </Router>
    </div>
  );
};

render(<App />, document.getElementById("root"));
