import axios from "axios";

export function fetchSessions() {
  return axios
    .get("http://www.mocky.io/v2/5dbc047b3100008dc44c0e0c")
    .then((response) => {
      return response.data.data.sessionIds.map((item) => {
        return {
          id: item,
          user: item.includes("Ext-")
            ? `${item.split("-")[1]}-${item.split("-")[2]}`
            : item.split("-")[1],
          datetime: new Date(parseInt(item.split("-")[0])).toLocaleString(),
        };
      });
    });
}

export function fetchHeartbeats(id) {
  return axios
    .get(
      `https://api.instant-rekall.dazndev.com/v1/getheartbeats?sessionId=${id}&offset=0&size=50`
    )
    .then((response) => {
      return response.data.data.heartbeats;
    });
}
