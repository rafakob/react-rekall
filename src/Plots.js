import React, { useEffect, useState } from "react";
import {
  LineChart,
  Line,
  CartesianGrid,
  XAxis,
  YAxis,
  Tooltip,
  Brush,
} from "recharts";
import { Button } from "antd";
import crossfilter from "crossfilter2";

const Plots = ({ heartbeatsDimension }) => {
  const [data, setData] = useState({});
  const [windowStartIndex, setWindowStartIndex] = useState(0);
  const [windowEndIndex, setWindowEndIndex] = useState(120);




  useEffect(() => {

    // plotData.remove((d, i) => {
    //     return i > windowStartIndex && i < windowEndIndex
    // })
    // console.log(plotData.allFiltered()[0])
      console.log(heartbeatsDimension)
      heartbeatsDimension.filter([windowStartIndex, windowEndIndex]);

      console.log(`windowStartIndex: ${windowStartIndex} | windowEndIndex: ${windowEndIndex}`)
      // console.log(heartbeatsDimension.bottom(Infinity))
    setData(heartbeatsDimension.bottom(Infinity));
  }, [windowStartIndex, windowEndIndex]);

  // useEffect( ()=>{
  //     const plotData = playbackMetrics.map((it) => ({
  //         timestamp: (it.timestamp - startTime) / 1000,
  //         playbackPosition: it.playbackPosition,
  //         estimatedBandwidth: it.estimatedBandwidth,
  //         bufferedDuration: it.bufferedDuration.video,
  //     }));
  //
  //     setTimeData(plotData);
  // }, [])

  return (
    <div className="plots">
      <Button
        type="primary"
        onClick={() => {
          setWindowStartIndex(windowStartIndex - 120);
          setWindowEndIndex(windowEndIndex - 120);
        }}
      >{`<-`}</Button>
      <Button
        type="primary"
        onClick={() => {
          setWindowStartIndex(windowStartIndex + 120);
          setWindowEndIndex(windowEndIndex + 120);
        }}
      >{`->`}</Button>

      <LineChart
        syncId="anyId"
        width={window.innerWidth}
        height={200}
        data={data}
        margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
      >
        <Line
          type="monotone"
          dataKey="playbackPosition"
          stroke="#3A5B20"
          dot={false}
          isAnimationActive={false}
        />
        <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
        <XAxis
          dataKey="timestamp"
          domain={[windowStartIndex, windowEndIndex]}
          type="number"
          unit="s"
        />
        <YAxis />
        <Tooltip />
      </LineChart>

      <LineChart
        syncId="anyId"
        width={window.innerWidth}
        height={300}
        data={data}
        margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
      >
        <Line
          type="monotone"
          dataKey="estimatedBandwidth"
          stroke="#8884d8"
          dot={false}
          isAnimationActive={false}
        />
        <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
        <XAxis
          dataKey="timestamp"
          domain={[windowStartIndex, windowEndIndex]}
          type="number"
          unit="s"
        />
        <YAxis />
        <Tooltip />
      </LineChart>

      <LineChart
        syncId="anyId"
        width={window.innerWidth}
        height={300}
        data={data}
        margin={{ top: 5, right: 20, bottom: 5, left: 0 }}
      >
        <Line
          type="monotone"
          dataKey="bufferedDuration"
          stroke="#F46075"
          dot={false}
          isAnimationActive={false}
        />
        <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
        <XAxis
          dataKey="timestamp"
          domain={[windowStartIndex, windowEndIndex]}
          type="number"
          unit="s"
        />
        <YAxis />
        <Tooltip />
      </LineChart>
    </div>
  );
};

export default Plots;
