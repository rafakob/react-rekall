import React, {useEffect, useState} from "react";
import Plots from "./Plots";
import crossfilter from "crossfilter2";
import {Spin} from "antd";

const Crossfilter = ({heartbeats}) => {
    const [dim, setDimension] = useState({})
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const playbackMetrics = heartbeats
            .map((it) => JSON.parse(it))
            .map((it) => it.events)
            .flat()
            .filter((it) => it.type === "PLAYBACK_METRICS");

        const list = heartbeats.map((item, index) => JSON.parse(item));

        const startTime = list[0].events[0].timestamp;
        const plotData = crossfilter(
            playbackMetrics.map((it) => ({
                timestamp: (it.timestamp - startTime) / 1000,
                playbackPosition: it.playbackPosition,
                estimatedBandwidth: it.estimatedBandwidth,
                bufferedDuration: it.bufferedDuration.video,
            }))
        );

        const dim = plotData.dimension(d => d.timestamp);

        setDimension(dim)
        setLoading(false)

    }, [])

    return (
        <div className="crossfilter">
            {loading
                ? <p key="plotting">plotting...</p>
                : <Plots heartbeatsDimension={dim}/>}
        </div>
    )

}

export default Crossfilter
